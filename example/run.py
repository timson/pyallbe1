from pyallbe1 import Controller
import time

DONGLE_CONNECTED = False

def print_sensor_data(msg):
    print('I got', msg)

def print_founded_allbe1(msg):
    print('I found', msg)

def print_dongle_disconnect():
    global DONGLE_CONNECTED
    print('Dongle disconnect')
    DONGLE_CONNECTED = False



if __name__ == '__main__':
    ctrl = Controller('COM4', scan_cb=print_founded_allbe1, sensor_cb=print_sensor_data, dongle_disconnect_cb=print_dongle_disconnect)
    print('Init done')
    devs = {}
    DONGLE_CONNECTED = True
    ctrl.disconnect_all()
    while len(list(devs.keys())) != 1:
        print('Start scanning', end=' ')
        code, devs = ctrl.scan()
        print('Found ({}) device(s)'.format(len(devs)))


    for k,v in list(devs.items()):
        status = ctrl.connect(k)
        print('Connect to {}: {}'.format(k, status))
        time.sleep(1)
        code, settings = ctrl.read_settings(k)
        print('Read settings from {} with status {}'.format(k, status))
        print('Settings', settings)
        update_settings = False
        if settings['PIR'] == 0:
            settings['PIR'] = 1
            update_settings = True
        if settings['TEMPERATURE'] == 0:
            settings['TEMPERATURE'] = 1
            update_settings = True
        if settings['UV'] == 0:
            settings['UV'] = 1
            update_settings = True
        if update_settings:
            res = ctrl.write_settings(k, settings)
            print('Update settings {} with status {}'.format(k, res))
        time.sleep(1)

    for k,v in list(devs.items()):
        print('sensor_control {}'.format(k), ctrl.sensor_control(k, enabled=True))
        time.sleep(1)

    for k,v in list(devs.items()):
        print('start_measurement {}'.format(k), ctrl.start_measurement(k, enabled=True))


    time.sleep(1)
    code, settings = ctrl.read_settings(k)
    print('PRE', settings)
    settings['PIR'] = 0
    time.sleep(1)
    res = ctrl.write_settings(k, settings)
    print('Update settings {} with status {}'.format(k, res))
    time.sleep(1)
    code, settings = ctrl.read_settings(k)
    print('POST', settings)

    for _ in range(30):
        if DONGLE_CONNECTED is False:
            break
        time.sleep(1)

    for k, v in list(devs.items()):
        print('start_measurement {}'.format(k), ctrl.start_measurement(k, enabled=False))

    time.sleep(1)

    for k, v in list(devs.items()):
        print(ctrl.disconnect(k))
        time.sleep(5)
    ctrl.stop()
