import pyallbe1
import time

def print_sensor_data(msg):
    print('I got', msg)

def print_founded_allbe1(msg):
    print('I found', msg)

ADDR = '6ceceb4c7073'

ctrl = pyallbe1.Controller('COM4', scan_cb=print_founded_allbe1, sensor_cb=print_sensor_data)
time.sleep(1)
print('Init done, now disconnect old devices')
ctrl.disconnect_all()
time.sleep(5)
print('Ready to work')

status = None
while status != pyallbe1.OK:
    print('Trying to connect')
    time.sleep(3)
    status = ctrl.connect(ADDR)

print('Connected to {}'.format(ADDR))
time.sleep(1)

if status == pyallbe1.OK:
    code, settings = ctrl.read_settings(ADDR)
    print('Read settings from {} with status {}: {}'.format(ADDR, status, settings))
    settings['PASSWORD'] = b'\x06\x02\x03\x04\x05\x06'
    settings['UV'] = 1 if settings['UV'] == 0 else 0
    time.sleep(1)
    print(ctrl.write_settings(ADDR, settings))
    time.sleep(1)
    code, settings = ctrl.read_settings(ADDR)
    print('Read settings from {} with status {}: {}'.format(ADDR, status, settings))

ctrl.stop()
