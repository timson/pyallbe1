from setuptools import setup, find_packages
setup(
    name='pyallbe1',
    version='0.0.1',
    install_requires=[
        'pyserial',
    ],
    packages=find_packages(),
    url='',
    license='',
    author='Timothy Sleptsov',
    author_email='tim@tslp.me',
    description='Python library for talking with allbe1 device through CC2540 dongle'
)