from pyallbe1.ble_builder import BLEBuilder
from pyallbe1.ble_parser import BLEParser
from collections import defaultdict
import collections
import serial
import time
import sys
import struct
import binascii
from threading import Lock, Event

OK = 0
GOT_TIMEOUT = 1
NOT_CONNECTED = 2
ALREADY_CONNECTED = 3
CONNECTION_FAIL = 4
NO_HANDLE_FOUND = 5
ERROR = 99

BAUDRATE = 57600
DEFAULT_TIMEOUT = 10
SCAN_TIMEOUT = 30
INIT_TIMEOUT = 10


def get_error(errno):
    """
    Return text description of error
    """
    errors = {
        OK: 'OK',
        GOT_TIMEOUT: 'GOT_TIMEOUT',
        NOT_CONNECTED: 'NOT_CONNECTED',
        ALREADY_CONNECTED: 'ALREADY_CONNECTED',
        ERROR: 'ERROR',
        NO_HANDLE_FOUND: 'NO_HANDLE_FOUND',
        CONNECTION_FAIL: 'CONNECTION_FAIL'
    }
    return errors.get(errno, 'UNKNOWN ERROR')


def handle_serial_exception(func):
    def wrap(*args, **kw):
        try:
            return func(*args, **kw)
        except serial.SerialException:
            self = args[0]
        if self._dongle_disconnect_cb:
            self._dongle_disconnect_cb()
    return wrap


class MyEvent(object):
    """
    Custom event class
    """

    def __init__(self):
        self.value = None
        self._event = Event()

    def set(self):
        """Set event
        """
        return self._event.set()

    def clear(self):
        """Clear event
        """
        return self._event.clear()

    def wait(self, timeout=DEFAULT_TIMEOUT):
        """Wait on event
        """
        return self._event.wait(timeout)


BATTERY_TABLE = {
    0x0: 'BAD BATTERY',
    0x1: 'CHARGE COMPLETE',
    0x2: 'CHARGING',
    0x3: 'POWER DOWN'
}


# def parse_allbe1_settings(msg):
#     """
#     Parse allbe1 settings
#     """
#     return {
#         'PIR': ord(msg[0]),
#         'MOVEMENTS': ord(msg[1]),
#         'UV': ord(msg[2]),
#         'TEMPERATURE': ord(msg[3]),
#         'BATTERY': BATTERY_TABLE.get(ord(msg[4]), ord(msg[4])),
#         'TRACKER': ord(msg[5]),
#         'FITNESS': ord(msg[6]),
#         'RESET_MIN': ord(msg[7]),
#         'PASSWORD': msg[8:13]
#     }

def parse_allbe1_settings(msg):
    """
    Parse allbe1 settings
    """
    return {
        'PIR': (msg[0]),
        'MOVEMENTS': (msg[1]),
        'UV': (msg[2]),
        'TEMPERATURE': (msg[3]),
        'BATTERY': BATTERY_TABLE.get((msg[4]), (msg[4])),
        'TRACKER': (msg[5]),
        'FITNESS': (msg[6]),
        'RESET_MIN': (msg[7]),
        'PASSWORD': msg[8:14]
    }


def build_allbe1_settings(settings):
    """
    Build allbe1 settings
    """
    output = struct.pack('BBBBBBBB',
                         settings['PIR'],
                         settings['MOVEMENTS'],
                         settings['UV'],
                         settings['TEMPERATURE'],
                         1,
                         settings['TRACKER'],
                         settings['FITNESS'],
                         settings['RESET_MIN'])
    output = bytearray(output)
    output.extend(settings['PASSWORD'])
    output.extend(b'\x00' * 6)
    return output


def hex_to_bin(value):
    """
    hex2bin convertor
    """
    tmp = value
    output = []
    while tmp:
        output.append(tmp[:2])
        tmp = tmp[2:]
    return ''.join([chr(int(x, 16)) for x in reversed(output)])


# def parse_allbe1_sensors(msg):
#     """
#     Parse info from allbe1 sensors
#     """
#     return {
#         'PIR_VALUE': ord(msg[0]),
#         'PIR_MSB': ord(msg[1]),
#         'PIR LSB': ord(msg[2]),
#         'MOVEMENTS_ALERT': ord(msg[3]),
#         'MOVEMENTS_MSB': ord(msg[4]),
#         'MOVEMENTS_LSB': ord(msg[5]),
#         'UV_INDEX': ord(msg[6]),
#         'TEMPERATURE': ord(msg[7]),
#         'KEY': ord(msg[8]),
#         'BATTERY_LEVEL': ord(msg[9]),
#         'FITNESS MSB': ord(msg[10]),
#         'FITNESS MSB': ord(msg[11]),
#         'CHARGING_MODE': ord(msg[12]),
#     }

def parse_allbe1_sensors(msg):
    """
    Parse info from allbe1 sensors
    """
    return {
        'PIR_VALUE': (msg[0]),
        'PIR_MSB': (msg[1]),
        'PIR LSB': (msg[2]),
        'MOVEMENTS_ALERT': (msg[3]),
        'MOVEMENTS_MSB': (msg[4]),
        'MOVEMENTS_LSB': (msg[5]),
        'UV_INDEX': (msg[6]),
        'TEMPERATURE': (msg[7]),
        'KEY': (msg[8]),
        'BATTERY_LEVEL': (msg[9]),
        'FITNESS MSB': (msg[10]),
        'FITNESS MSB': (msg[11]),
        'CHARGING_MODE': (msg[12]),
    }


class Controller(object):
    """
    Dongle controller class
    """

    def __init__(self, port_name, scan_cb=None, sensor_cb=None, disconnect_cb=None, dongle_disconnect_cb=None, rssi_cb=None):
        self.serial_port = serial.Serial(port=port_name, baudrate=BAUDRATE)
        self.ble_builder = BLEBuilder(self.serial_port)
        self._dongle_disconnect_cb = dongle_disconnect_cb
        self.ble_parser = BLEParser(self.serial_port, callback=self._packet_handler, serial_exception_cb=dongle_disconnect_cb)

        self.event_callbacks = {
            'GAP_DeviceInitDone': self._cp2540_init_done,
            'GAP_DeviceDiscoveryDone': self._scanning_done,
            'GAP_EstablishLink': self._link_established,
            'GAP_LinkTerminated': self._link_terminated,
            'GAP_DeviceInformation': self._device_information,
            'ATT_ReadRsp': self._read_response,
            'ATT_HandleValueNotification': self._handle_notification,
            'ATT_WriteRsp': self._write_response,
            'HCI_ReadRSSI': self._read_rssi
        }
        self._ready = False
        self._scanning = False
        self._lock = Lock()

        self._scan_cb = scan_cb
        self._sensor_cb = sensor_cb
        self._disconnect_cb = disconnect_cb
        self._rssi_cb = rssi_cb

        self.events = {
            'INIT': MyEvent(),
            'SCAN': MyEvent(),
            'READ': defaultdict(MyEvent),
            'WRITE': defaultdict(MyEvent),
            'CONNECT': defaultdict(MyEvent),
            'DISCONNECT': defaultdict(MyEvent)
        }

        self._devices = defaultdict(dict)
        self._connections = {}
        self._conn_handles = {}
        self._init_cp2540()

    def _init_cp2540(self):
        with self._lock:
            self.ble_builder.send("fe00")
            event = self.events['INIT']
            event.wait()
            event.clear()

    def _cp2540_init_done(self, msg):
        event = self.events['INIT']
        event.value = self._is_success(msg)
        event.set()

    def _is_success(self, msg):
        _, status = msg.get('status', None)
        return status == '00'

    def scan(self):
        """Start scanning proccess for new BLE devices
        """
        with self._lock:
            self.ble_builder.send("fe04", mode=b"\x03")
            self._devices = defaultdict(dict)
            event = self.events['SCAN']
            res = event.wait(timeout=SCAN_TIMEOUT)
            event.clear()
            if not res:
                return GOT_TIMEOUT, {}
            return OK, {k:str(binascii.b2a_qp(v)) for k, v in event.value.items()}

    def devices(self):
        """Get dictionary with founded BLE devices

        Where key is address of device and value is custom data_field
        """
        return {k: v.get('data_field', '') for k, v in list(self._devices.items())}

    def _device_information(self, msg):
        bin_addr, addr = msg['addr']
        self._devices[addr]['bin_addr'] = bin_addr
        data = msg['data_field'][0]
        if self._scan_cb:
            self._scan_cb({'addr': addr, 'data': data})
        self._devices[addr]['data_field'] = data

    def _scanning_done(self, msg):
        _, num_dev = msg['num_devs']
        num_dev = int(num_dev)
        if num_dev:
            for device in msg['devices']:
                bin_addr, addr = device['addr']
                self._devices[addr]['bin_addr'] = bin_addr
        event = self.events['SCAN']
        event.value = self.devices()
        event.set()

    @handle_serial_exception
    def connect(self, addr):
        """Connect to Allbe1 device

        """
        with self._lock:
            # device = self._devices.get(addr, None)
            # bin_addr = hex_to_bin(addr)
            bin_addr = bytearray.fromhex(addr)[::-1]
            if addr not in self._connections:
                self.ble_builder.send("fe09", peer_addr=bin_addr)
                event = self.events['CONNECT'][addr]
                res = event.wait()
                event.clear()
                if not res:
                    return GOT_TIMEOUT
                elif res is False:
                    return CONNECTION_FAIL
                self.events['CONNECT'].pop(addr, None)
                return OK
            else:
                return ALREADY_CONNECTED

    @handle_serial_exception
    def disconnect_all(self):
        """
        Disconnect all allbe1 devices
        """
        with self._lock:
            self.ble_builder.send("fe0a", conn_handle=b'\xFF\xFF')
            return OK

    @handle_serial_exception
    def disconnect(self, addr):
        """Disonnect from Allbe1 device

        """
        with self._lock:
            device = self._connections.get(addr, None)
            if device:
                if device['status'] == 'connected':
                    handle = device['handle']
                    self.ble_builder.send("fe0a", conn_handle=handle)
                    event = self.events['DISCONNECT'][addr]
                    res = event.wait()
                    event.clear()
                    if not res:
                        return GOT_TIMEOUT
                    self.events['DISCONNECT'].pop(addr, None)
                    return OK
            return NOT_CONNECTED

    @handle_serial_exception
    def getRSSI(self, addr):
        with self._lock:
            device = self._connections.get(addr, None)
            if device:
                if device['status'] == 'connected':
                    handle = device['handle']
                    self.ble_builder.send("1405", conn_handle=handle)

    def _link_established(self, msg):
        _, addr = msg['dev_addr']
        if self._is_success(msg):
            conn_handle = msg['conn_handle'][1]
            self._connections[addr] = {
                'status': 'connected',
                'info': msg,
                'handle': msg['conn_handle'][0]
            }
            self._conn_handles[conn_handle] = addr
        else:
            self._connections.pop(addr, None)
        event = self.events['CONNECT'].get(addr, None)
        if event:
            event.value = self._is_success(msg)
            event.set()

    def _link_terminated(self, msg):
        if self._is_success(msg):
            conn_handle = msg['conn_handle'][1]
            addr = self._conn_handles.get(conn_handle, None)
            if addr:
                self._conn_handles.pop(conn_handle)
                self._connections.pop(addr)
                event = self.events['DISCONNECT'].get(addr, None)
                if event:
                    event.value = True
                    event.set()
                if self._disconnect_cb:
                    self._disconnect_cb(addr)

    def _packet_handler(self, *args):
        _, msg = args[0]
        _, event_type = msg.get('event', (None, None))
        if event_type:
            func = self.event_callbacks.get(event_type, None)
            if func:
                func(msg)
        else:
            _, event_code = msg.get('event_code', (None, None))
            if event_code:
                func = self.event_callbacks.get(event_code, None)
                if func:
                    func(msg)

    def _read_response(self, msg):
        if self._is_success(msg):
            conn_handle = msg['conn_handle'][1]
            settings = msg['value'][0]
            addr = self._conn_handles.get(conn_handle, None)
            if addr:
                event = self.events['READ'].get(addr, None)
                if event:
                    event.value = parse_allbe1_settings(settings)
                    event.set()

    def _write_response(self, msg):
        if self._is_success(msg):
            conn_handle = msg['conn_handle'][1]
            addr = self._conn_handles.get(conn_handle, None)
            if addr:
                event = self.events['WRITE'].get(addr, None)
                if event:
                    event.set()

    def _read_rssi(self, msg):
        if self._is_success(msg):
            conn_handle = msg['handle'][1]
            addr = self._conn_handles.get(conn_handle, None)
            if addr:
                if self._rssi_cb:
                    self._rssi_cb({'addr': addr, 'rssi': ord(msg.get('rssi')[0])})

    def _handle_notification(self, msg):
        if self._is_success(msg):
            values = msg['values'][0]
            conn_handle = msg['conn_handle'][1]
            addr = self._conn_handles.get(conn_handle, None)
            if addr:
                sensor_data = parse_allbe1_sensors(values)
                if self._sensor_cb:
                    self._sensor_cb({'addr': addr, 'data': sensor_data})

    @handle_serial_exception
    def read_settings(self, addr):
        """Read settings from Allbe1 device
        """
        with self._lock:
            connection = self._connections.get(addr, None)
            if connection:
                handle = connection.get('handle', None)
                if handle:
                    self.ble_builder.send(
                        "fd8a", conn_handle=handle, handle=b'\x4a\x00')
                    event = self.events['READ'][addr]
                    res = event.wait(DEFAULT_TIMEOUT)
                    self.events['READ'].pop(addr, None)
                    if not res:
                        return GOT_TIMEOUT, None
                    else:
                        return OK, event.value
                else:
                    return NO_HANDLE_FOUND, None
            else:
                return NOT_CONNECTED, None

    @handle_serial_exception
    def _write_request(self, addr, handle, cmd):
        with self._lock:
            connection = self._connections.get(addr, None)
            if connection:
                conn_handle = connection['handle']
                event = self.events['WRITE'][addr]
                self.ble_builder.send(
                    "fd92", conn_handle=conn_handle, handle=handle, value=cmd)
                res = event.wait()
                if not res:
                    return GOT_TIMEOUT
                return OK
            else:
                return NOT_CONNECTED

    def write_settings(self, addr, settings):
        """
        Write settings to Allbe1 device
        """
        cmd = build_allbe1_settings(settings)
        return self._write_request(addr, b'\x47\x00', cmd)

    def sensor_control(self, addr, enabled):
        """
        Sensor control
        """
        cmd = b'\x01\x00' if enabled else b'\x00\x00'
        return self._write_request(addr, b'\x47\x00', cmd)

    def start_measurement(self, addr, enabled):
        """
        Start measurement
        """
        cmd = b'\x01' if enabled else b'\x00'
        return self._write_request(addr, b'\x4a\x00', cmd)

    def stop(self):
        """
        Stop class isinstance
        """
        self.ble_parser.stop()
